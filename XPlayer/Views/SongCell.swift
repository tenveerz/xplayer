//
//  SongCell.swift
//  XPlayer
//
//  Created by Tanveer on 4/8/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import UIKit
import Kingfisher

class SongCell: UICollectionViewCell {

    @IBOutlet var createdAgo: UILabel!
    @IBOutlet var authorName: [UILabel]!
    @IBOutlet var authorImageView: UIImageView!
    @IBOutlet var elapsedTime: UILabel!
    @IBOutlet var albumType: UILabel!
    @IBOutlet var songTitle: UILabel!
    @IBOutlet var songImageView: UIImageView!
    @IBOutlet weak var playpauseImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.18).cgColor
        layer.shadowOffset = CGSize(width: 0, height: 2.0)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 2.0
        layer.masksToBounds = false
        authorImageView.layer.cornerRadius = authorImageView.frame.size.width / 2;
        authorImageView.layer.masksToBounds = true
        elapsedTime.isHidden = true
    }
    
    override func prepareForReuse() {
        playpauseImageView.isHighlighted = false
        elapsedTime.isHidden = true
    }
    
    
    func configureWith(sectionViewModel: SongSectionViewModelType){
        albumType.text = "# Hip Hop"
        songImageView.kf.setImage(with: sectionViewModel.song.picture?.medium)
        songTitle.text = sectionViewModel.song.name
        authorImageView.kf.setImage(with: sectionViewModel.song.author?.picture?.small)
        for authorNameLabel in authorName{
            authorNameLabel.text = sectionViewModel.song.author?.name
        }
        createdAgo.text = sectionViewModel.song.createdPresentable
        
        if sectionViewModel.cellState == .Playing{
            playpauseImageView.isHighlighted = true
        }else{
            playpauseImageView.isHighlighted = false
        }

        if sectionViewModel.cellState == .Playing && sectionViewModel.songStats.playedTime > 0{
            elapsedTime.isHidden = false
            elapsedTime.text = sectionViewModel.songStats.playedTime.MinSecFormat() + " / " + sectionViewModel.songStats.totalTime.MinSecFormat()
        }else{
            elapsedTime.isHidden = true
        }

        
    }
    
    

}
