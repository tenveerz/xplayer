//
//  SongStats.swift
//  XPlayer
//
//  Created by Tanveer on 16/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation


struct SongStats {
    var playedTime: Int
    var totalTime: Int
}
