//
//  SongState.swift
//  XPlayer
//
//  Created by Tanveer on 16/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation

enum SongState {
    case Playing
    case Paused
    case NotPlaying
}
