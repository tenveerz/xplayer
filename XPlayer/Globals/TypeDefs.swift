//
//  TypeDefs.swift
//  XPlayer
//
//  Created by Tanveer on 4/8/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation

typealias voidParameterVoidReturn = () -> Void
typealias SongArrayParameterVoidReturn = ([SongType]) -> Void
typealias SongStateParameterVoidReturn = (SongState) -> Void
