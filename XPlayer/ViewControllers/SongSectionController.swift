//
//  SongSectionController.swift
//  XPlayer
//
//  Created by Tanveer on 4/8/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import IGListKit


class SongSectionController: IGListSectionController, IGListSectionType {
    
    var viewModel: SongSectionViewModelType
    init(viewModel:SongSectionViewModelType) {
        self.viewModel = viewModel
        super.init()
        inset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
        if let viewController = viewController as? CoordinatAble{
            viewController.coordinator.add(delegate: self)
        }

    }

    deinit {
        if let viewController = viewController as? CoordinatAble{
            viewController.coordinator.remove(delegate: self)
        }
    }
    
    func numberOfItems() -> Int {
        return 1
    }
    
    func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 300)
    }
    
    func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: "SongCell", bundle: nil, for: self, at: index) as! SongCell
        cell.configureWith(sectionViewModel: viewModel)
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        return cell
    }
    
    func didUpdate(to object: Any) {
        if let viewModel = object as? SongSectionViewModelType{
            self.viewModel = viewModel
        }
    }
    
    func didSelectItem(at index: Int) {
        
        
        if let viewController = viewController as? CoordinatAble{
            if viewModel.cellState == .NotPlaying{
                viewController.coordinator.play(song: viewModel.song)
            }else if viewModel.cellState == .Paused{
                viewController.coordinator.resume(song: viewModel.song)
            }else{
                viewController.coordinator.pause()
            }
        }
    }
    
    func refreshUI() {
        let cells = collectionContext!.visibleCells(for: self)
        for cell in cells{
            if let songCell = cell as? SongCell{
                songCell.configureWith(sectionViewModel: viewModel)
            }
            
        }

    }
    
}

extension SongSectionController: PlayerDelegate{

    func startedPlaying(song: SongType){
        if let cellSongID = viewModel.song.id, let songID = song.id{
            if songID == cellSongID{
                viewModel.cellState = .Playing
                print("play tapped inside section \(songID)")
            }else{ // some other song item is being tapped to play
                viewModel.cellState = .NotPlaying
            }

            refreshUI()
        }

    }
    func paused(song: SongType){
        if let cellSongID = viewModel.song.id, let songID = song.id{
            if songID == cellSongID{
                viewModel.cellState = .Paused
                print("pause tapped inside section \(viewModel.song.id)")
            }else{
                viewModel.cellState = .NotPlaying
            }
            refreshUI()
        }

    }
    func resumed(song: SongType){}
    
    func time(song: SongType, songStats: SongStats){
        if let cellsongID = viewModel.song.id, let songID = song.id, cellsongID == songID{
            viewModel.songStats.playedTime = songStats.playedTime
            viewModel.songStats.totalTime = songStats.totalTime
            refreshUI()
        }
    }


}


