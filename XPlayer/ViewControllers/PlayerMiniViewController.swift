//
//  PlayerMiniViewController.swift
//  XPlayer
//
//  Created by Tanveer on 4/8/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import UIKit


class PlayerMiniViewController: UIViewController {

    @IBOutlet var authorTitle: UILabel!
    @IBOutlet var songTitle: UILabel!
    @IBOutlet var actionButton: UIButton!
    
    var viewModel: PlayerMiniViewModelType {
        didSet{
            resetUI()
        }
    }
    
    weak var delegate: PlayerMiniViewControllerDelegate?
    
    
    init(viewModel: PlayerMiniViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: "PlayerMiniViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewController()
    }

    func setupViewController(){
        authorTitle.text = viewModel.authorName
        songTitle.text = viewModel.songName
        actionButton.isSelected = true
    }
    
    func resetUI(){
        setupViewController()
    }
    
    func pauseSong(album: SongType) {
        actionButtonTapped(actionButton)
    }
    
    
    @IBAction func actionButtonTapped(_ sender: Any) {
        if let button = sender as? UIButton{
            if button.isSelected{
                delegate?.pauseTapped(songID: viewModel.songID)
            }else{
                delegate?.playTapped(songID: viewModel.songID)
            }
            
            
            button.isSelected = !button.isSelected
        }
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        delegate?.openFullScreenPlayer()
    }
    

    
   
}
