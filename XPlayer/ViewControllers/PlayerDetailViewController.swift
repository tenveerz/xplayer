//
//  PlayerDetailViewController.swift
//  XPlayer
//
//  Created by Tanveer on 4/17/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import UIKit
import Kingfisher


class PlayerDetailViewController: UIViewController, CoordinatAble {

    
    
    @IBOutlet var screenTitle: UILabel!
    @IBOutlet var songCover: UIImageView!
    @IBOutlet var songTitle: UILabel!
    @IBOutlet var songArtist: UILabel!
    @IBOutlet var songPlayedDuration: UILabel!
    @IBOutlet var songTotalDuration: UILabel!
    @IBOutlet var songDurationSlider: UISlider!
    @IBOutlet var playPauseButton: UIButton!
    @IBOutlet var previousButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    
    var viewModel: PlayerDetailViewModelType
    var coordinator: CoordinatorType
    
    init(viewModel: PlayerDetailViewModelType, coordinator: CoordinatorType) {
        self.viewModel = viewModel
        self.coordinator = coordinator
        super.init(nibName: "PlayerDetailViewController", bundle: nil)
        self.viewModel.songStateChangeSignal = songStateChanged
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        coordinator.remove(delegate: self)
    }
    
    func setupViewController() {
        coordinator.add(delegate: self)
        refreshUI()
    }
    
    func refreshSlider() {
        songPlayedDuration.text = viewModel.songStats?.playedTime.MinSecFormat()
        songTotalDuration.text = viewModel.songStats?.totalTime.MinSecFormat()
        songDurationSlider.minimumValue = 0
        if let maxSongTime = viewModel.songStats?.totalTime {
            songDurationSlider.maximumValue = Float(maxSongTime)
        }
        if let currentTime = viewModel.songStats?.playedTime {
            songDurationSlider.value = Float(currentTime)
        }
    }
    
    func refreshUI() {
        screenTitle.text = "Top Hits Singapore"
        songCover.kf.setImage(with: viewModel.song.picture?.medium)
        songTitle.text = viewModel.song.name
        songArtist.text = viewModel.song.author?.name
        refreshSlider()
    }
    

    @IBAction func songDurationValueChanged(_ sender: Any) {
        if let slider = sender as? UISlider{
            coordinator.seekTo(sec: Int(slider.value))
            
        }
    }
    
    @IBAction func playPauseButtonTapped(_ sender: Any) {
        if let button = sender as? UIButton{
            if button.isSelected{
                coordinator.pause()
            }else{
                coordinator.resume(song: viewModel.song)
            }
        }
    }
    @IBAction func previousButtonTapped(_ sender: Any) {
        coordinator.playPrevious()
    }
    @IBAction func nextButtonTapped(_ sender: Any) {
        coordinator.playNext()
    }
    

    @IBAction func closePlayer(_ sender: Any) {
        coordinator.closeFullPlayer()
    }

    func songStateChanged(state: SongState){
        if state == SongState.Paused{
            playPauseButton.isSelected = false
        }else{
            playPauseButton.isSelected = true
        }
   
    }
    

}


extension PlayerDetailViewController: PlayerDelegate{
    
    func startedPlaying(song: SongType){
        viewModel.song = song
        viewModel.songState = .Playing
        refreshUI()
    }
    func paused(song: SongType){
        viewModel.songState = .Paused
    }

    func resumed(song: SongType){}
    
    func time(song: SongType, songStats: SongStats){
            viewModel.songStats = songStats
            refreshSlider()
    }
    
    
}



