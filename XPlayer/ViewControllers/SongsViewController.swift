//
//  ViewController.swift
//  XPlayer
//
//  Created by Tanveer on 4/3/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import UIKit
import IGListKit

class SongsViewController: UIViewController,CoordinatAble {

    @IBOutlet var collectionView: IGListCollectionView!
    lazy var adapter: IGListAdapter = {
        return IGListAdapter(updater: IGListAdapterUpdater(), viewController: self, workingRangeSize: 0)
    }()

    // Ideally will be passed from from calss who is initialzing this viewcontroller
    var viewModel: SongsViewModelType = SongsViewModel(albumAPI: SongAPI())
    var coordinator: CoordinatorType = Coordinator()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()

    }

    func setupController() {
        viewModel.refreshUISignal = refreshAdapter
        viewModel.playlistLoadedSignal = populatePlayerWith
        adapter.collectionView = collectionView
        adapter.dataSource = viewModel
        coordinator.navigationController = self.navigationController
        viewModel.loadSongs()
    }
    
    
    func refreshAdapter(){
        adapter.performUpdates(animated: true, completion: nil)
    }
    
    func populatePlayerWith(playList: [SongType]){
        coordinator.populatePlayerWith(playList: playList)
    }
    
}


