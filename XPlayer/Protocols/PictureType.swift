//
//  Picture.swift
//  XPlayer
//
//  Created by Tanveer on 4/3/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation

protocol PictureType {
    var small: URL? {get set}
    var medium: URL? {get set}
    var large: URL? {get set}
    var extraSmall: URL? {get set}
    var url: URL? {get set}
}
