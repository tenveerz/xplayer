//
//  SongType.swift
//  XPlayer
//
//  Created by Tanveer on 4/3/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import IGListKit

protocol SongType: CreatedPresentable, IGListDiffable {
    
    var author: AuthorType? {get set}
    var createdOn: Date? {get set}
    var modifiedOn: Date? {get set}
    var picture: PictureType? {get set}
    var audioLink: URL? {get set}
    var id: String? {get set}
    var name: String? {get set}
    
}
