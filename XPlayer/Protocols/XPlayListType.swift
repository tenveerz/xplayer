//
//  XPlayListType.swift
//  XPlayer
//
//  Created by Tanveer on 16/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
protocol XPlayListType{
    func addToPlayList(playerItem: XPlayerItem)
    func addToPlayList(collection: [XPlayerItem])
    func playerItem(songID: String) -> XPlayerItem?
    func clearPlayList()
    func playListCount() -> Int
    func playerItem(atIndex: Int) -> XPlayerItem?
    func indexOf(songID: String) -> Int?
    
    
}
