//
//  PlayerMiniViewModelType.swift
//  XPlayer
//
//  Created by Tanveer on 16/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation

protocol PlayerMiniViewModelType {
    var songName: String? {get set}
    var authorName: String? {get set}
    var songID: String? {get set}
}

