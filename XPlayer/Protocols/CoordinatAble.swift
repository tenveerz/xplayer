//
//  CoordinatAble.swift
//  XPlayer
//
//  Created by Tanveer on 4/8/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation

protocol CoordinatAble {
    
    var coordinator: CoordinatorType {get set}
}
