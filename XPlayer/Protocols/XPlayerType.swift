//
//  XPlayerType.swift
//  XPlayer
//
//  Created by Tanveer on 16/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation

protocol XPlayerType {
    var delegate: XPlayerDelegate? {get set}
    func play(atIndex: Int)
    func currentSeekInSecs() -> Float64
    func seekTo(sec: Int64)
    func currentPlayerItem() -> XPlayerItem?
    func currentPlayerItemIndex() -> Int?
    func resume()
    func pause()
    func play()
    func playPrevious()
    func playNext()
    func playWith(songID: String)
    func addToPlayList(collection: [XPlayerItem])
    func addToPlayList(playerItem: XPlayerItem)
    
    
    
}
