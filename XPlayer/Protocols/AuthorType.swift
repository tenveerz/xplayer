//
//  AuthorType.swift
//  XPlayer
//
//  Created by Tanveer on 4/4/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation

protocol AuthorType {
    
    var name: String? {get set}
    var picture: PictureType? {get set}
}
