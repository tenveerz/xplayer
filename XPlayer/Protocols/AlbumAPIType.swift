//
//  SongAPIType.swift
//  XPlayer
//
//  Created by Tanveer on 16/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import BoltsSwift


protocol SongAPIType {
    func getSongs() -> Task<[SongType]>
}
