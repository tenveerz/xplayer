//
//  CoordinatorType.swift
//  XPlayer
//
//  Created by Tanveer on 16/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import UIKit


protocol CoordinatorType {
    var navigationController: UINavigationController? {get set}
    var player: XPlayerType {get set}
    func add(delegate: PlayerDelegate)
    func remove(delegate: PlayerDelegate)
    func play(song: SongType)
    func pause()
    func resume(song: SongType)
    func populatePlayerWith(playList: [SongType])
    func seekTo(sec: Int)
    func playNext()
    func playPrevious()
    func closeFullPlayer()

    
}
