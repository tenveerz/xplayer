//
//  SongsViewModelType.swift
//  XPlayer
//
//  Created by Tanveer on 16/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import IGListKit

protocol SongsViewModelType: IGListAdapterDataSource {
    var albumAPI: SongAPIType {get set}
    var refreshUISignal: voidParameterVoidReturn? {get set}
    var playlistLoadedSignal: SongArrayParameterVoidReturn? {get set}
    var albums: [SongType] {get}
    func loadSongs()
}

