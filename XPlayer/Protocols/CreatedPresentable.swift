//
//  CreatedOnPresentable.swift
//  XPlayer
//
//  Created by Tanveer on 4/8/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import DateToolsSwift

protocol CreatedPresentable {
    var createdOn: Date? {get set}
}

extension CreatedPresentable{


    
    var createdPresentable: String?{
        if let created = createdOn{
            let days = created.daysAgo
            let hours = created.hoursAgo
            let mins = created.minutesAgo
            
            if days > 7{
                return created.format(with: "MMMM d, yyyy h a")
            }else if days >= 1 && days <= 7  {
                return "\(days) \(days == 1 ? "day": "days") ago"
            }else if hours >= 1 && hours <= 23{
                return "\(hours) \(hours == 1 ? "hour": "hours") ago"
            }else if mins >= 0{
                return "\(mins) \(mins == 1 ? "min": "mins") ago"
            }
            
        }
        return nil
    }

    
    

}
