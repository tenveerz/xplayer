//
//  SongSectionViewModelType.swift
//  XPlayer
//
//  Created by Tanveer on 16/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import IGListKit


protocol SongSectionViewModelType: IGListDiffable{
    
    var song: SongType {get set}
    var cellState: SongState {get set}
    var songStats: SongStats {get set}
    
}


