//
//  PlayerMiniViewControllerViewModel.swift
//  XPlayer
//
//  Created by Tanveer on 4/8/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation



class PlayerMiniViewModel: PlayerMiniViewModelType {
    
    var songName: String?
    var authorName: String?
    var songID: String?

    convenience init(album: SongType) {
        self.init()
        songName = album.name
        authorName = album.author?.name
        songID = album.id
    }
    
    
    
    
}
