//
//  SongsViewModel.swift
//  XPlayer
//
//  Created by Tanveer on 4/8/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import IGListKit
import BoltsSwift


class SongsViewModel: NSObject, SongsViewModelType {
    
    var albumAPI: SongAPIType
    var refreshUISignal: voidParameterVoidReturn?
    var playlistLoadedSignal: SongArrayParameterVoidReturn?
    var albums: [SongType] {
        return _albums
    }
    private var _albums: [SongType] = [SongType]()
    init(albumAPI: SongAPIType) {
        self.albumAPI = albumAPI
    }
    
    func loadSongs() {
        albumAPI.getSongs().continueWith {[weak self] (task:Task<[SongType]>) -> Void in
            if let error = task.error{
                print(error.localizedDescription)
            }else if let albums = task.result {
                self?._albums.append(contentsOf: albums)
                self?.playlistLoadedSignal?(albums)
                self?.refreshUISignal?()
            }
        }

    }
    
    
}




// MARK: - IGListAdapterDataSource
extension SongsViewModel: IGListAdapterDataSource {
    
    func objects(for listAdapter: IGListAdapter) -> [IGListDiffable] {
        return albums
    }
    
    func listAdapter(_ listAdapter: IGListAdapter, sectionControllerFor object: Any) -> IGListSectionController {

        if let viewModel = object as? SongType{
            return SongSectionController(viewModel: SongSectionViewModel(song: viewModel, cellState: .NotPlaying, songStats: SongStats(playedTime: 0, totalTime: 0)))
        }
        
        assert(true, "Must imlement section controller for \(object)")
        return IGListSectionController() // to silence the compiler
    }
    
    func emptyView(for listAdapter: IGListAdapter) -> UIView? { return nil }
}
