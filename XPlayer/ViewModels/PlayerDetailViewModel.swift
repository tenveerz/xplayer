//
//  PlayerDetailViewModelType.swift
//  XPlayer
//
//  Created by Tanveer on 4/17/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
protocol PlayerDetailViewModelType {
    
    var song: SongType {get set}
    var songStats: SongStats? {get set}
    var songState: SongState {get set}
    var songStateChangeSignal: SongStateParameterVoidReturn? {get set}
}


class PlayerDetailViewModel: PlayerDetailViewModelType{
    
    var song: SongType
    var songStats: SongStats?
    var songStateChangeSignal: SongStateParameterVoidReturn?
    var songState: SongState = SongState.Playing{
        didSet{
            songStateChangeSignal?(songState)
        }
    }
    
    init(song: SongType) {
        self.song = song
    }
    
}
