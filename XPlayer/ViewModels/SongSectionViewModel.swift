//
//  SongSectionViewModel.swift
//  XPlayer
//
//  Created by Tanveer on 15/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import IGListKit

class SongSectionViewModel: NSObject, SongSectionViewModelType, IGListDiffable {

    var song: SongType
    var cellState: SongState
    var songStats: SongStats
    
    
    init(song: SongType, cellState: SongState, songStats: SongStats) {
        self.song = song
        self.cellState = cellState
        self.songStats = songStats
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        if let id = song.id{
            return id as NSObjectProtocol
        }else{
            return self
        }
        
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? SongSectionViewModel else { return false }
        if let id = song.id , let pid = object.song.id{
            return id == pid
            && cellState == object.cellState
            && songStats.playedTime == object.songStats.playedTime
            && songStats.totalTime == object.songStats.totalTime
        }else{
            return true
        }
    }


}
