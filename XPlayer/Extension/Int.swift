//
//  Int.swift
//  XPlayer
//
//  Created by Tanveer on 16/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation

extension Int{
    
    func format(format: String) -> String {
        return String(format: format, self)
    }
    
    func MinSecFormat() -> String {
        let minutes = self / 60
        let remainingSecs = self % 60
        
        let formatSpecifier = "%02d"
        return "\(minutes):" + remainingSecs.format(format: formatSpecifier)
    }
    
}



