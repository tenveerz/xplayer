//
//  SongsCache.swift
//  XPlayer
//
//  Created by Tanveer on 4/17/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation


class SongsCache: SongsCacheType{
    
    var songs: [SongType] = [SongType]()
    
    
    func add(songs: [SongType]){
        self.songs.append(contentsOf: songs)
    }
    
    func song(for ID: String) -> SongType? {

        return songs.filter { (song) -> Bool in
            if let songID = song.id, songID == ID{
                return true
            }
            else{
                return false
            }
        }.first
    }
    

    
}
