//
//  Songs.swift
//  XPlayer
//
//  Created by Tanveer on 4/4/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import ObjectMapper

class Songs: SongsType, Mappable {
    
    var albums: [SongType] {
        if let albums = _albums{
            return albums
        }else{
            return [SongType]()
        }
    }
    
    private var _albums: [Song]?
    
    
    required init?(map: Map) {
        
    }
    
    
    // Mappable
    func mapping(map: Map) {
        _albums    <- map["data"]
    }

    
}
