//
//  Author.swift
//  XPlayer
//
//  Created by Tanveer on 4/4/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import ObjectMapper

class Author: AuthorType, Mappable {
    
    
    var name: String?
    var picture: PictureType? {
        set{
            _picture = newValue as? Picture
        }
        get{
            return _picture
        }
    }

    private var _picture: Picture?
    
    required init?(map: Map) {
        
    }
    
    
    // Mappable
    func mapping(map: Map) {
        name    <- map["name"]
        _picture         <- map["picture"]
    }

}
