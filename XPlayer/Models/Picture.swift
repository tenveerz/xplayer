//
//  Picture.swift
//  XPlayer
//
//  Created by Tanveer on 4/3/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import ObjectMapper

class Picture: PictureType, Mappable {
    
    var small: URL?
    var medium: URL?
    var large: URL?
    var extraSmall: URL?
    var url: URL?
    
    required init?(map: Map) {
        
    }

    
    // Mappable
    func mapping(map: Map) {
        small    <- (map["s"], URLTransform())
        medium         <- (map["m"], URLTransform())
        large      <- (map["l"], URLTransform())
        extraSmall       <- (map["xs"], URLTransform())
        url  <- (map["url"], URLTransform())
    }
    
}
