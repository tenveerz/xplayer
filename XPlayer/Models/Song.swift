//
//  SongType.swift
//  XPlayer
//
//  Created by Tanveer on 4/3/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import ObjectMapper
import IGListKit

class Song: NSObject, SongType, Mappable, IGListDiffable {
    

    var createdOn: Date?
    var modifiedOn: Date?

    var audioLink: URL?
    var id: String?
    var name: String?

    var author: AuthorType?{
        set{
            _author = newValue as? Author
        }
        get{
            return _author
        }
    }
    var picture: PictureType?{
        set{
            _picture = newValue as? Picture
        }
        get{
            return _picture
        }
    }

    private var _author: Author?
    private var _picture: Picture?
    
    
    
    required init?(map: Map) {
        
    }
    
    
    // Mappable
    func mapping(map: Map) {
        _author    <- map["author"]
        createdOn    <- (map["createdOn"], XDateTransform())
        modifiedOn    <- (map["modifiedOn"], XDateTransform())
        _picture         <- map["picture"]
        audioLink      <- (map["audioLink"], URLTransform())
        id         <- map["id"]
        name         <- map["name"]
    }

    
    
    
    public func diffIdentifier() -> NSObjectProtocol {
        if let id = id{
            return id as NSObjectProtocol
        }else{
            return self
        }
        
    }
    
    public func isEqual(toDiffableObject object: IGListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? Song else { return false }
        if let id = id , let pid = object.id{
            return id == pid
        }else{
            return true
        }
    }

    
}
