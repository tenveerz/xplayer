//
//  Coordinator.swift
//  XPlayer
//
//  Created by Tanveer on 4/8/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import UIKit


class Coordinator: CoordinatorType {


    var navigationController: UINavigationController?

    
    fileprivate let playerMulticastDelegate = MulticastDelegate<PlayerDelegate>()
    fileprivate var playerMini: PlayerMiniViewController?
    internal var player: XPlayerType = XPlayer()
    fileprivate var cache: SongsCache = SongsCache()
    init() {
        player.delegate = self
    }
    
}

extension Coordinator{
    
    func populatePlayerWith(playList: [SongType]){
        cache.add(songs: playList)
        for song in playList{
            if let songID = song.id, let songURL = song.audioLink{
                player.addToPlayList(playerItem: XPlayerItem(url: songURL, songID: songID))
            }
        }
    }
    func add(delegate: PlayerDelegate) {
        playerMulticastDelegate.add(delegate)
    }
    
    func remove(delegate: PlayerDelegate) {
        playerMulticastDelegate.remove(delegate)
    }

    
    func play(song: SongType) {
        if let songID = song.id{
            player.playWith(songID: songID)
            showMiniPlayer(song: song)
        }
    }
    
    func pause() {
        player.pause()
        removeMiniPlayer()
    }
    
    func resume(song: SongType){
        player.resume()
        showMiniPlayer(song: song)
    }

    func seekTo(sec: Int){
        player.seekTo(sec: Int64(sec))
        
    }

    func playNext(){
        player.playNext()
    }
    func playPrevious(){
        player.playPrevious()
    }

    
    fileprivate func showMiniPlayer(song: SongType){
        
        let miniplayerHeight: CGFloat = 60
        let viewModel = PlayerMiniViewModel(album: song)
        if let playerMini = playerMini{
            playerMini.viewModel = viewModel
        }else if let navigationController = navigationController{
            let playerMini = PlayerMiniViewController(viewModel: viewModel)
            playerMini.view.frame = CGRect(x: 0, y: navigationController.view.bounds.height - miniplayerHeight, width: navigationController.view.bounds.width, height: miniplayerHeight)
            navigationController.view.addSubview(playerMini.view)
            playerMini.delegate = self
            self.playerMini = playerMini
        }
    }

    
    
    fileprivate func removeMiniPlayer(){
        playerMini?.view.removeFromSuperview()
        playerMini = nil
    }
    
    func closeFullPlayer() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
}


extension Coordinator : PlayerMiniViewControllerDelegate{
    
    func playTapped(songID: String?){
        player.resume()
    }
    
    func pauseTapped(songID: String?){
        player.pause()
        removeMiniPlayer()
    }
 
    func openFullScreenPlayer(){
        guard let currentPlayerItem = player.currentPlayerItem() else{ return }
        guard let song = cache.song(for: currentPlayerItem.songID) else {return }
        let viewModel: PlayerDetailViewModelType = PlayerDetailViewModel(song: song)
        let playerController = PlayerDetailViewController(viewModel: viewModel, coordinator: self)
        navigationController?.present(playerController, animated: true, completion: nil)

    }

}



extension Coordinator: XPlayerDelegate{

    func time(songID: String, songStats: SongStats){
        guard let currentPlayerItem = player.currentPlayerItem() else{ return }
        guard let song = cache.song(for: currentPlayerItem.songID) else {return }

        playerMulticastDelegate.invoke { (delegate) in
            delegate.time(song: song, songStats: songStats)
        }
        
    }
    func startedPlaying(songID: String){
        guard let currentPlayerItem = player.currentPlayerItem() else{ return }
        guard let song = cache.song(for: currentPlayerItem.songID) else {return }

        playerMulticastDelegate.invoke { (delegate) in
            delegate.startedPlaying(song: song)
        }
    }
    func paused(songID: String){
        guard let currentPlayerItem = player.currentPlayerItem() else{ return }
        guard let song = cache.song(for: currentPlayerItem.songID) else {return }
        playerMulticastDelegate.invoke { (delegate) in
            delegate.paused(song: song)
        }
    }
    func resumed(songID: String){
        guard let currentPlayerItem = player.currentPlayerItem() else{ return }
        guard let song = cache.song(for: currentPlayerItem.songID) else {return }

        playerMulticastDelegate.invoke { (delegate) in
            delegate.startedPlaying(song: song)
        }
}

}
