//
//  SongAPI.swift
//  XPlayer
//
//  Created by Tanveer on 4/4/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import BoltsSwift
import Alamofire
import ObjectMapper


class SongAPI: SongAPIType {
    
    let url: String = "https://gist.githubusercontent.com/anonymous/fec47e2418986b7bdb630a1772232f7d/raw/5e3e6f4dc0b94906dca8de415c585b01069af3f7/57eb7cc5e4b0bcac9f7581c8.json"
    

    func getSongs() -> Task<[SongType]>{
    
    let taskCompletionSource = TaskCompletionSource<[SongType]>()

        Alamofire.request(url, method: .get)
            .responseJSON { response in
    
            if (response.result.error == nil) {
                if let responseData = response.result.value as? [String: Any], let albums = Mapper<Songs>().map(JSON: responseData){
                    taskCompletionSource.set(result: albums.albums)
                }else{
                    taskCompletionSource.cancel()
                }
            }else {
                taskCompletionSource.set(error: response.result.error!)
            }
        }

        return taskCompletionSource.task
    
    }

    
    
    
    
}
