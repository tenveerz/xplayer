//
//  XPlayer.swift
//  XPlayer
//
//  Created by Tanveer on 4/9/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import AVFoundation


class XPlayer: XPlayerType {

    private var player = AVQueuePlayer()
    private var playerItem:AVPlayerItem?
    private var timeObserver : ((CMTime) -> Swift.Void )?
    private var playList: XPlayListType = XPlayList()
    weak var delegate: XPlayerDelegate?
    private var playingSongIndex: Int?
    
    init() {
        
        player.actionAtItemEnd = .none
        
        weak var weakSelf = self
           timeObserver = { (CMTime) -> Void in
                if weakSelf?.player.currentItem?.status == .readyToPlay {
                    if let currentItem = weakSelf?.player.currentItem as? XPlayerItem , let currentTime = weakSelf?.player.currentTime(), let totalTime = weakSelf?.player.currentItem?.asset.duration{
                        let time : Float64 = CMTimeGetSeconds(currentTime)
                        let totalTime : Float64 = CMTimeGetSeconds(totalTime)
                        let songStats = SongStats(playedTime: Int(time), totalTime: Int(totalTime))
                        weakSelf?.delegate?.time(songID: currentItem.songID, songStats: songStats)
                    }
                }
        }
        

        // Adding observers for time and song finished
        player.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, 1), queue: DispatchQueue.main, using: timeObserver!)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(XPlayer.songFinished(_:)),
                                               name: .AVPlayerItemDidPlayToEndTime,
                                               object: player.currentItem)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        if let timeObserver = timeObserver{
            player.removeTimeObserver(timeObserver)
        }
    }

    
    func addToPlayList(playerItem: XPlayerItem){
        playList.addToPlayList(playerItem: playerItem)
    }
    func addToPlayList(collection: [XPlayerItem]) {
        playList.addToPlayList(collection: collection)
    }
    
    private func clearAllPlaylist() {
        player.removeAllItems()
        playList.clearPlayList()
    }
    
    @objc func songFinished(_ notification: NSNotification) {
        print("Song finihsed")
    }
    
    func playWith(songID: String) {
        if let index = playList.indexOf(songID: songID){
            play(atIndex: index)
        }
    }
    
    func playNext() {
        
        var nextSongIndex: Int = 0 // if no playing item start from 0
        if let playingSongIndex = playingSongIndex{
            nextSongIndex = playingSongIndex + 1
        }

        // if last item them start from 0
        if nextSongIndex >= playList.playListCount(){
            nextSongIndex = 0
        }

        play(atIndex: nextSongIndex)
    }
    
    func playPrevious() {

        var previousSongIndex: Int = 0 // if no playing song start from 0
        if let playingSongIndex = playingSongIndex{
            if currentSeekInSecs() < 3 { // if less than 3 secs play previous song
                previousSongIndex = playingSongIndex - 1
            }else{ // otherwise play the same song from start
                previousSongIndex = playingSongIndex
            }
        }
        
        // if no previous item them play from last
        if previousSongIndex < 0 {
            let lastSongIndex = playList.playListCount() - 1
            if  lastSongIndex > 0{
                previousSongIndex = lastSongIndex
            }
        }

        play(atIndex: previousSongIndex)
        
    }
    
    func play()  {
        player.play()
        if let currentItem = player.currentItem as? XPlayerItem{
            delegate?.startedPlaying(songID: currentItem.songID)
        }
        
    }
    

    func pause() {
        player.pause()
        if let currentItem = player.currentItem as? XPlayerItem{
            delegate?.paused(songID: currentItem.songID)
        }
    }

    func resume() {
        if player.rate == 0{
            player.play()
            if let currentItem = player.currentItem as? XPlayerItem{
                delegate?.resumed(songID: currentItem.songID)
            }
        }
    }
    
    func currentPlayerItemIndex() -> Int? {
        if let currentItem = player.currentItem as? XPlayerItem{
            return playList.indexOf(songID: currentItem.songID)
        }
        return nil
    }

    func currentPlayerItem() -> XPlayerItem? {
        return player.currentItem as? XPlayerItem
    }

    
    func seekTo(sec: Int64){
        let targetTime:CMTime = CMTimeMake(sec, 1)
        player.seek(to: targetTime)

    }
    
    func currentSeekInSecs() -> Float64 {
        let time:Float64 = CMTimeGetSeconds(player.currentTime())
        return time
    }
    
    func play(atIndex: Int) {
        playingSongIndex = atIndex
        player.removeAllItems()
        let playlistCount = playList.playListCount()
        var index = atIndex
        for _ in atIndex...playlistCount{
            if let playerItem = playList.playerItem(atIndex: index), player.canInsert(playerItem, after: nil){
                playerItem.seek(to: kCMTimeZero)
                    player.insert(playerItem, after: nil)
            }
            index = index + 1
        }
        
        play()
        
    }
    


}
