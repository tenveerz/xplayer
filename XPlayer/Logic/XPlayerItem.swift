//
//  XPlayerItem.swift
//  XPlayer
//
//  Created by Tanveer on 16/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation
import AVFoundation

class XPlayerItem: AVPlayerItem{
    
    var songID: String
    
    init(url: URL, songID: String) {
        self.songID = songID
        super.init(asset: AVAsset(url: url), automaticallyLoadedAssetKeys: nil)
    }
    
    var songURL: URL? {
        
        if let asset = self.asset as? AVURLAsset{
            return asset.url
        }
        
        return nil
        
    }
    
    
}
