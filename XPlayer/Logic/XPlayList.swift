//
//  XPlayList.swift
//  XPlayer
//
//  Created by Tanveer on 16/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation

class XPlayList: XPlayListType{
    private var playList: [XPlayerItem] = [XPlayerItem]()
    
    func addToPlayList(playerItem: XPlayerItem){
        playList.append(playerItem)
    }
    func addToPlayList(collection: [XPlayerItem]) {
        playList.append(contentsOf: collection)
    }
    
    
    func playerItem(songID: String) -> XPlayerItem? {
        return playList.filter { (xPlayerItem) -> Bool in
            return xPlayerItem.songID == songID
            }.first
    }
    
    func clearPlayList() {
        playList.removeAll()
    }
    
    func playListCount() -> Int{
        return playList.count
    }
    
    func playerItem(atIndex: Int) -> XPlayerItem?{
        guard playList.count > 0 else { return nil }
        
        if atIndex >= 0  && atIndex < playList.count{
            return playList[atIndex]
        }else{
            return nil
        }
    }
    
    func indexOf(songID: String) -> Int? {
        var index = -1
        for playerItem in playList{
            index = index + 1
            if playerItem.songID == songID {return index}
        }
        return  nil
    }
    
}
