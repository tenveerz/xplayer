//
//  XPlayerDelegate.swift
//  XPlayer
//
//  Created by Tanveer on 16/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation

protocol XPlayerDelegate: class {
    
    func time(songID: String, songStats: SongStats)
    func startedPlaying(songID: String)
    func paused(songID: String)
    func resumed(songID: String)
    
    
}

