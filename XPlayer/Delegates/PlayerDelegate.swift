//
//  PlayerDelegate.swift
//  XPlayer
//
//  Created by Tanveer on 4/17/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation

protocol PlayerDelegate: class {
    
    func time(song: SongType, songStats: SongStats)
    func startedPlaying(song: SongType)
    func paused(song: SongType)
    func resumed(song: SongType)
    
    
}

