//
//  PlayerMiniViewControllerDelegate.swift
//  XPlayer
//
//  Created by Tanveer on 16/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation

protocol PlayerMiniViewControllerDelegate: class {
    func playTapped(songID: String?)
    func pauseTapped(songID: String?)
    func openFullScreenPlayer()
}

