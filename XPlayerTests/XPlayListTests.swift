//
//  XPlayListTests.swift
//  XPlayer
//
//  Created by Tanveer on 14/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import XCTest
@testable import XPlayer

class XPlayListTests: XCTestCase {
    
    let playerItem0 = XPlayerItem(url: URL(string: "https://a.clyp.it/iv1xl24p.mp3")!, songID: "0")
    let playerItem1 = XPlayerItem(url: URL(string: "https://a.clyp.it/mbk0eyht.mp3")!, songID: "1")
    let playerItem2 = XPlayerItem(url: URL(string: "https://a.clyp.it/y0hvfibi.mp3")!, songID: "2")
    let playerItem3 = XPlayerItem(url: URL(string: "https://a.clyp.it/pezpcfah.mp3")!, songID: "3")
    let playerItem4 = XPlayerItem(url: URL(string: "https://a.clyp.it/ce5uo2mz.mp3")!, songID: "4")

    var playlist: XPlayListType!
    override func setUp() {
        super.setUp()
        playlist = XPlayList()
        playlist.addToPlayList(collection: [playerItem0, playerItem1, playerItem2, playerItem3, playerItem4])
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testList(){
    
        XCTAssert(playlist.playListCount() == 5, "count must be 5")
        XCTAssert(playlist.indexOf(songID: "1")! == 1, "index of song ID 1 shoube be 1")
        XCTAssert(playlist.indexOf(songID: "100") == nil, "index of song ID 100 shoube be nil")
        XCTAssert(playlist.playerItem(atIndex: 3)!.songID == "3" , "song ID at index 3 should be 3")
        XCTAssert(playlist.playerItem(atIndex: 100) == nil , "play item at index 100 should be nil")
        XCTAssert(playlist.playerItem(atIndex: -1) == nil , "play item at index -1 should be nil")
        XCTAssert(playlist.playerItem(songID: "3")!.songID == "3" , "song ID at index 3 should be 3")
        XCTAssert(playlist.playerItem(songID: "100") == nil , "song ID at index 100 should be nil")
        playlist.clearPlayList()
        XCTAssert(playlist.playListCount() == 0, "count must be 0")
        XCTAssert(playlist.indexOf(songID: "1") == nil, "empty playlist should return nil")
        XCTAssert(playlist.playerItem(atIndex: 3) == nil, "empty playlist should return nil")
        XCTAssert(playlist.playerItem(atIndex: -1) == nil , "empty playlist should return nil")
        XCTAssert(playlist.playerItem(songID: "3") == nil, "empty playlist should return nil")

    }
    
}
