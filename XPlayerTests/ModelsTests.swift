//
//  ModelsTests.swift
//  XPlayer
//
//  Created by Tanveer on 4/4/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import XCTest
import ObjectMapper

@testable import XPlayer



class ModelsTests: XCTestCase {
    
    
    var albums: SongsType = {
        let json = BundleFileLoader().loadMockData(forResource: "Songs", ofType: "json")
        return Mapper<Songs>().map(JSON: json as! [String: Any])!

    }()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSongsModel() {
        XCTAssert(albums.albums.count == 5, "The number of albums should be 5")
    }

    func testSongModel() {
        let album = albums.albums.first!
        XCTAssert(album.audioLink!.absoluteString == "https://a.clyp.it/iv1xl24p.mp3", "The Song of audioLink is wrong")
        XCTAssert(album.picture!.small!.absoluteString == "https://bandlab-test-images.azureedge.net/v1.0/Users/71c81538-4e88-e511-80c6-000d3aa03fb0/636016633463395803/360x360", "The Song Picture is wrong")
        XCTAssert(album.id == "0", "The Song ID is wrong")
    }

    func testAuthorModel() {
        let author = albums.albums.first!.author!
        XCTAssert(author.name == "Aleks", "The Author Name is wrong")
        XCTAssert(author.picture!.small!.absoluteString == "https://bandlab-test-images.azureedge.net/v1.0/Users/71c81538-4e88-e511-80c6-000d3aa03fb0/636016633463395803/360x360", "The Author Picture is wrong")
    }

    func testPictureModel() {
        let picture = albums.albums.first!.author!.picture!
        XCTAssert(picture.small!.absoluteString == "https://bandlab-test-images.azureedge.net/v1.0/Users/71c81538-4e88-e511-80c6-000d3aa03fb0/636016633463395803/360x360", "The Author Picture S is wrong")
        XCTAssert(picture.medium!.absoluteString == "https://bandlab-test-images.azureedge.net/v1.0/Users/71c81538-4e88-e511-80c6-000d3aa03fb0/636016633463395803/640x640", "The Author Picture M is wrong")
        XCTAssert(picture.large!.absoluteString == "https://bandlab-test-images.azureedge.net/v1.0/Users/71c81538-4e88-e511-80c6-000d3aa03fb0/636016633463395803/1024x1024", "The Author Picture L is wrong")
        XCTAssert(picture.extraSmall!.absoluteString == "https://bandlab-test-images.azureedge.net/v1.0/Users/71c81538-4e88-e511-80c6-000d3aa03fb0/636016633463395803/100x100", "The Author Picture XS is wrong")
        XCTAssert(picture.url!.absoluteString == "https://bandlab-test-images.azureedge.net/v1.0/users/71c81538-4e88-e511-80c6-000d3aa03fb0/636016633463395803/", "The Author Picture URL is wrong")
    }


    
}
