//
//  CreatedPresentableTests.swift
//  XPlayer
//
//  Created by Tanveer on 4/8/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import XCTest
import DateToolsSwift
@testable import XPlayer

class Created: CreatedPresentable {
    var createdOn: Date?

}




class CreatedPresentableTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
 
    
    func testArticeCreatedTimePresentable() {
        let createdObject = Created()

        createdObject.createdOn = 2.minutes.earlier
        XCTAssert(createdObject.createdPresentable == "2 mins ago", "createdPresentable error, should be 2 mins ago")
        createdObject.createdOn = 1.minutes.earlier
        XCTAssert(createdObject.createdPresentable == "1 min ago", "createdPresentable error, should be 1 min ago")
        createdObject.createdOn = 2.hours.earlier
        XCTAssert(createdObject.createdPresentable == "2 hours ago", "createdPresentable error, should be 2 hours ago")
        createdObject.createdOn = 1.hours.earlier
        XCTAssert(createdObject.createdPresentable == "1 hour ago", "createdPresentable error, should be 1 hour ago")
        createdObject.createdOn = 2.days.earlier
        XCTAssert(createdObject.createdPresentable == "2 days ago", "createdPresentable error, should be 2 days ago")
        createdObject.createdOn = 1.days.earlier
        XCTAssert(createdObject.createdPresentable == "1 day ago", "createdPresentable error, should be 1 day ago")
        createdObject.createdOn = Date(year: 2017, month: 3, day: 23).subtract(8.days)
        XCTAssert(createdObject.createdPresentable == "March 15, 2017 12 AM", "createdPresentable error, should be 2 mins ago")
        createdObject.createdOn = Date().subtract(7.days)
        XCTAssert(createdObject.createdPresentable == "7 days ago", "createdPresentable error, should be 7 days ago")
        createdObject.createdOn = Date()
        XCTAssert(createdObject.createdPresentable == "0 mins ago", "createdPresentable error, should be 0 mins ago")
    }

    
    
}
