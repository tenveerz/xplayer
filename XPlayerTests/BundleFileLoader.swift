//
//  BundleFileLoader.swift
//  XPlayer
//
//  Created by Tanveer on 4/4/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import Foundation

class BundleFileLoader: NSObject {
    
    
    func loadMockData(forResource: String, ofType: String) -> [AnyHashable: Any]{
        let json = Bundle(for: type(of: self)).path(forResource: forResource, ofType: ofType)
        do {
            let jsonDict = try JSONSerialization.jsonObject(with: Data(contentsOf: URL(fileURLWithPath: json!)),
                                                            options: JSONSerialization.ReadingOptions.allowFragments)
            return jsonDict as! [AnyHashable: Any]
            
        } catch {
            return ["Dummy":"Dummy"]
        }
        
    }
    
    
}
