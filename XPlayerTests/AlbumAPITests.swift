//
//  SongAPITests.swift
//  XPlayer
//
//  Created by Tanveer on 4/4/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import XCTest
import OHHTTPStubs
import ObjectMapper
import BoltsSwift

@testable import XPlayer

class SongAPITests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetNews() {
        let albums = BundleFileLoader().loadMockData(forResource: "Songs", ofType: "json")
        OHHTTPStubs.stubRequests(passingTest: { _ in
            return true
        }, withStubResponse:  { _ in
            return OHHTTPStubsResponse(jsonObject: albums,
                                       statusCode: 200,
                                       headers: ["content-type": "application/json"])
        })
        
        let expectation = self.expectation(description: "GET {{url}}/raw/")
        SongAPI().getSongs().continueWith { (task:Task<[SongType]>) -> Void in
            XCTAssert(task.result!.count == 5, "Songs should be 5")
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 1, handler: nil)
        
        
    }
    
    
}
