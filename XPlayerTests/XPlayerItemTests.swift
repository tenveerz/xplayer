//
//  XPlayerItemTests.swift
//  XPlayer
//
//  Created by Tanveer on 14/04/2017.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import XCTest
@testable import XPlayer

class XPlayerItemTests: XCTestCase {
    
    let playerItem0 = XPlayerItem(url: URL(string: "https://a.clyp.it/iv1xl24p.mp3")!, songID: "0")
    let playerItem1 = XPlayerItem(url: URL(string: "https://a.clyp.it/mbk0eyht.mp3")!, songID: "1")
    let playerItem2 = XPlayerItem(url: URL(string: "https://a.clyp.it/y0hvfibi.mp3")!, songID: "2")
    let playerItem3 = XPlayerItem(url: URL(string: "https://a.clyp.it/pezpcfah.mp3")!, songID: "3")
    let playerItem4 = XPlayerItem(url: URL(string: "https://a.clyp.it/ce5uo2mz.mp3")!, songID: "4")
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    

    func testSongID(){
        XCTAssert(playerItem0.songID == "0", "Song ID should be 0")
        XCTAssert(playerItem1.songID == "1", "Song ID should be 1")
        XCTAssert(playerItem2.songID == "2", "Song ID should be 2")
    }

    func testSongURL(){
        XCTAssert(playerItem0.songURL!.absoluteString == "https://a.clyp.it/iv1xl24p.mp3", "Song ID should be 0")
        XCTAssert(playerItem1.songURL!.absoluteString == "https://a.clyp.it/mbk0eyht.mp3", "Song ID should be 1")
        XCTAssert(playerItem2.songURL!.absoluteString == "https://a.clyp.it/y0hvfibi.mp3", "Song ID should be 2")
    }
}
