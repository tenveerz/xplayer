//
//  PlayerTests.swift
//  XPlayer
//
//  Created by Tanveer on 4/11/17.
//  Copyright © 2017 Tanveer. All rights reserved.
//

import XCTest
@testable import XPlayer


class PlayerTests: XCTestCase {
    
    var player: XPlayerType!
    var mockDelegate : MockDelegate!
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        mockDelegate = MockDelegate()
        player = XPlayer()
        player.delegate = mockDelegate
        player.addToPlayList(playerItem: XPlayerItem(url: URL(string: "https://a.clyp.it/iv1xl24p.mp3")!, songID: "0"))
        player.addToPlayList(playerItem: XPlayerItem(url: URL(string: "https://a.clyp.it/mbk0eyht.mp3")!, songID: "1"))
        player.addToPlayList(playerItem: XPlayerItem(url: URL(string: "https://a.clyp.it/y0hvfibi.mp3")!, songID: "2"))
        player.addToPlayList(playerItem: XPlayerItem(url: URL(string: "https://a.clyp.it/pezpcfah.mp3")!, songID: "3"))
        player.addToPlayList(playerItem: XPlayerItem(url: URL(string: "https://a.clyp.it/ce5uo2mz.mp3")!, songID: "4"))
        
    }
    
    override func tearDown() {
        super.tearDown()
    }

    
    func testSongIndex0(){
        player.playWith(songID: "0")
        XCTAssert(player.currentPlayerItemIndex()! == 0 , "Player item index should be 0")
    }

    func testSongIndex1(){
        player.playWith(songID: "1")
        XCTAssert(player.currentPlayerItemIndex()! == 1 , "Player item index should be 1")
    }

    func testSongIndex2And3(){
        player.playWith(songID: "2")
        XCTAssert(player.currentPlayerItemIndex()! == 2 , "Player item index should be 2")
        player.playNext()
        XCTAssert(player.currentPlayerItemIndex()! == 3 , "Player item index should be 3")
        player.playPrevious()
        XCTAssert(player.currentPlayerItemIndex()! == 2 , "Player item index should be 2")
        player.seekTo(sec: 5)
        player.playPrevious()
        XCTAssert(player.currentPlayerItemIndex()! == 2 , "Player item index should be 2")
        player.seekTo(sec: 2)
        player.playPrevious()
        XCTAssert(player.currentPlayerItemIndex()! == 1 , "Player item index should be 1")
        player.playPrevious()
        XCTAssert(player.currentPlayerItemIndex()! == 0 , "Player item index should be 0")
        player.playPrevious()
        XCTAssert(player.currentPlayerItemIndex()! == 4 , "Player item index should be 4")
        player.playNext()
        XCTAssert(player.currentPlayerItemIndex()! == 0 , "Player item index should be 0")
    }
    
    func testSeek() {
        player.playWith(songID: "0")
        player.seekTo(sec: 5)
        XCTAssert(player.currentSeekInSecs() < 6 && player.currentSeekInSecs() > 4, "Seek should be around 5")
    }
    
    
    
    func testDelegate(){
        player.playWith(songID: "0")
        XCTAssert(mockDelegate.startedPlaying == true , "Delegate should have received started paying signal")
        XCTAssert(mockDelegate.songID == "0" , "Song ID should be 0")
        player.pause()
        XCTAssert(mockDelegate.paused == true , "Delegate should have received paused signal")
        XCTAssert(mockDelegate.songID == "0" , "Delegate should have received started paying signal")
        player.resume()
        XCTAssert(mockDelegate.resumed == true , "Delegate should have received resume signal")
        XCTAssert(mockDelegate.songID == "0" , "Delegate should have received started paying signal")
        player.playWith(songID: "3")
        XCTAssert(mockDelegate.startedPlaying == true , "Delegate should have received started paying signal")
        XCTAssert(mockDelegate.songID == "3" , "Song ID should be 3")
    }
    
}


class MockDelegate: XPlayerDelegate {
    
    var songID: String = ""
    var startedPlaying = false
    var paused = false
    var resumed = false
    
    
    func time(songID: String, songStats: SongStats){}
    func startedPlaying(songID: String){
        startedPlaying = true
        self.songID = songID
        
        paused = false
        resumed = false
    }
    func paused(songID: String){
        paused = true
        self.songID = songID

        startedPlaying = false
        resumed = false

    }
    func resumed(songID: String){
        resumed = true
        self.songID = songID
        startedPlaying = false
        paused = false
    }

    
    
}


